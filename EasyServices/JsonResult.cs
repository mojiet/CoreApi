﻿using System;
using System.Collections.Generic;

namespace EasyServices
{
    public class JsonResult<T>
    {
        public JsonResult()
        {
            Success = false;
            Message = string.Empty;
            Result = default(T);
        }
        public int Code { get; set; }
        //返回的结果
        public bool Success { get; set; }
        //返回的信息
        public string Message { get; set; }
        //返回的实体，可选
        public T Result { get; set; }

        public List<T> List { get; set; }

        public JsonResult<T> SetSuccess()
        {
            Code = 200;
            Success = true;
            Message = "操作成功";
            return this;
        }
        public JsonResult<T> SetSuccess(string msg)
        {
            Code = 200;
            Success = true;
            Message = msg;
            return this;
        }
        public JsonResult<T> SetSuccess(T entity)
        {
            Code = 200;
            Success = true;
            Message = "操作成功";
            Result = entity;
            return this;
        }
        public JsonResult<T> SetSuccess(List<T> list)
        {
            Code = 200;
            Success = true;
            Message = "操作成功";
            List = list;
            return this;
        }

        public JsonResult<T> SetError(string msg)
        {
            Code = 500;
            Message = msg;
            return this;
        }
        public JsonResult<T> SetError(string msg,int code)
        {
            Code = code;
            Message = msg;
            return this;
        }
    }
}
