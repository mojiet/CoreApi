﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EasyServices.Interfaces
{
    public interface IBaseService<T> where T : class
    {
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        Task<T> Get(Expression<Func<T,bool>> expression);
            /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task<JsonResult<T>> UpdateAsync(T entity);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task<JsonResult<T>> DeleteAsync(T entity);
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<JsonResult<T>> DeleteAsync(Expression<Func<T, bool>> expression);
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        Task<T> FindAsync(params object[] keyValues);

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        List<T> FindList(Expression<Func<T, bool>> expression);

        Task<List<T>> FindListAsync(Expression<Func<T, bool>> expression);
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <param name="orderBy">排序</param>
        /// <returns></returns>
        Task<List<T>> FindListAsync(Expression<Func<T, bool>> expression, Expression<Func<T, bool>> orderBy);
    }
}
