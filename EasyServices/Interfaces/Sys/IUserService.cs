﻿using System.Threading.Tasks;
using EasyEntity.Sys;

namespace EasyServices.Interfaces.Sys
{
    public interface IUserService : IBaseService<User>
    {
        Task<JsonResult<User>> LoginAsync(User entity);

        Task<JsonResult<User>> Register(User entity);

        Task<User> GetByLoginName(User entity);

    }
}
