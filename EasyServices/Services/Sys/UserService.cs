﻿using System;
using System.Threading.Tasks;
using EasyData;
using EasyEntity.Sys;
using EasyServices.Interfaces.Sys;

namespace EasyServices.Services.Sys
{
    public class UserService : ServiceBase<User>,IUserService
    {
        public UserService(CoreDbContext dbContext) : base(dbContext)
        {

        }

        
        public virtual async Task<JsonResult<User>> LoginAsync(User entity)
        {
            var result = new JsonResult<User>();
            try
            {
                var u = await Get(r => r.LoginName == entity.LoginName);
                if (u != null && entity.Password == u.Password)
                {
                    result.SetSuccess();
                }
            }
            catch (Exception e)
            {
                return result.SetError(e.Message);
               
            }

            return result;
        }

        public virtual async Task<JsonResult<User>> Register(User entity)
        {
            var result = new JsonResult<User>();
            if (await DbContext.SaveChangesAsync() > 0)
            {

            }
            return result;
        }

        public async Task<User> GetByLoginName(User entity)
        {
            var result = new User();
            if (entity != null)
            {
                result = await Get(r => r.LoginName == entity.LoginName);
            }
            return result;
        }
    }
}
