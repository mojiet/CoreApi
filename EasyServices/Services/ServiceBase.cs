﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EasyData;
using EasyEntity;
using EasyServices.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EasyServices.Services
{
    public class ServiceBase<T> : IBaseService<T> where T:BaseEntity 
    {
        protected CoreDbContext DbContext { get; set; }

        public ServiceBase(CoreDbContext dbContext)
        {
            DbContext = dbContext;
        }
        public virtual async  Task<bool> AnyAsync(Expression<Func<T, bool>> expression)
        {
            return await DbContext.Set<T>().AnyAsync(expression);
        }

        public virtual async Task<T> Get(Expression<Func<T, bool>> expression)
        {
           return await DbContext.Set<T>().FirstOrDefaultAsync(expression);
        }

        public virtual async Task<JsonResult<T>> UpdateAsync(T entity)
        {
            JsonResult<T> result = new JsonResult<T>();
            entity.PreUpdate();
            DbContext.Set<T>().Update(entity);
            if (await DbContext.SaveChangesAsync() > 0)
            {
                result.Success = true;
                result.Message = "保存数据成功";
            }
            else
            {
                result.Success = false;
                result.Message = "保存数据失败";
            }
            return result;
        }

        public virtual async Task<JsonResult<T>> DeleteAsync(T entity)
        {
            var operationResult = new JsonResult<T>();
            entity.PreDelete();
            DbContext.Set<T>().Update(entity);
            if (await DbContext.SaveChangesAsync() > 0)
            {
                operationResult.Success = true;
                operationResult.Message = "删除成功";
            }
            else
            {
                operationResult.Success = false;
                operationResult.Message = "删除失败";
            }
            return operationResult;
        }

        public virtual async Task<JsonResult<T>> DeleteAsync(Expression<Func<T, bool>> expression)
        {
            var operationResult = new JsonResult<T>();
            var list = FindList(expression);
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var t = list[i];
                    t.PreDelete();
                }
            }
            if (list != null) DbContext.Set<T>().UpdateRange(list);
            if (await DbContext.SaveChangesAsync() > 0)
            {
                operationResult.Success = true;
                operationResult.Message = "删除成功";
            }
            else
            {
                operationResult.Success = false;
                operationResult.Message = "删除失败";
            }
            return operationResult;
        }

        public virtual async Task<T> FindAsync(params object[] keyValues)
        {
            return await DbContext.Set<T>().FindAsync(keyValues);
        }

        public virtual List<T> FindList(Expression<Func<T, bool>> expression)
        {
            return  DbContext.Set<T>().AsNoTracking().Where(expression).ToList();
        }

        public virtual async Task<List<T>> FindListAsync(Expression<Func<T, bool>> expression)
        {
            return await DbContext.Set<T>().AsNoTracking().Where(expression).ToListAsync();
        }

        public virtual async Task<List<T>> FindListAsync(Expression<Func<T, bool>> expression, Expression<Func<T, bool>> orderBy)
        {
            return await DbContext.Set<T>().AsNoTracking().Where(expression).OrderBy(orderBy).ToListAsync();
        }
    }
}
