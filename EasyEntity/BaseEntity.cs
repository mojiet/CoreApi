﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EasyEntity
{
    public class BaseEntity : ICloneable
    {
        public BaseEntity()
        {
            DelFlag = false;
            CreateDate = DateTime.Now;
            CreateBy = 0;
        }
        public int Id { get; set; }
        public bool DelFlag { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }

        public void PreUpdate()
        {
            CreateDate = DateTime.Now;
        }

        public void PreDelete()
        {
            DelFlag = true;
            UpdateDate = DateTime.Now;
        }
        public void PreInsert()
        {
            CreateDate = DateTime.Now;
        }

        public object Clone()
        {
            return this;
        }
        [NotMapped]
        public int PageSize { get; set; }
        [NotMapped]
        public int PageNum { get; set; }
    }
}
