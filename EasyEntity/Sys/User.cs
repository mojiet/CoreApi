﻿namespace EasyEntity.Sys
{
    public class User : BaseEntity
    {
        public string LoginName { get; set; }
        public string RealName { get; set; }
        public string Avator { get; set; }
        public string Password { get; set; }
        public bool IsSystemData { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool IsLock { get; set; }
        public int Status { get; set; }
    }
}
