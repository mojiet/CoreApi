﻿namespace EasyEntity.Sys
{
    public class UserRole : BaseEntity
    {
        public UserRole()
        {
            IsSystemData = false;
        }
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
        public bool IsSystemData { get; set; }
    }
}
