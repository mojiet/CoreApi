﻿namespace EasyEntity.Sys
{
    public class RoleMenu :BaseEntity
    {
        public RoleMenu()
        {
            IsSystemData = false;
        }
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public bool IsSystemData { get; set; }
    }
}
