﻿namespace EasyEntity.Sys 
{
    public class SysLog : BaseEntity
    {
        public string Title { get; set; }
        public byte LogType { get; set; }
        public string RequestUrl { get; set; }
        public string Method { get; set; }
        public string Params { get; set; }
        public string UserAgent { get; set; }
    }
}
