﻿using System.ComponentModel.DataAnnotations;

namespace EasyEntity.Sys
{
    public class Menu : BaseEntity
    {
        public Menu()
        {
            ParentId = 0;
            Order = 0;
            IsSystemData = false;
        }
        public int ParentId { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public byte MenuType { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 排序越大越靠后
        /// </summary>
        public int Order { get; set; }

        public string Icon { get; set; }
        [MaxLength(50)]
        public string PermisionCode { get; set; }
        [MaxLength(100)]
        public string ParentIds { get; set; }
        public bool IsSystemData { get; set; }
        public string Component { get; set; }
    }
}
