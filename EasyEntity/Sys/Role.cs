﻿namespace EasyEntity.Sys
{  
    public class Role : BaseEntity
    {
        public Role()
        {
            IsSystemData = false;
        }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }

        public bool IsSystemData  { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
