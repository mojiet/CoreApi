﻿using System.Threading.Tasks;
using EasyEntity.Sys;
using EasyServices;
using EasyServices.Interfaces.Sys;
using Microsoft.AspNetCore.Mvc;

namespace EasyCore.Controllers.Sys
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("login")]
        public async Task<JsonResult<User>> LoginAsync(string userName,string password)
        {
            var u = new User()
            {
                LoginName = userName,
                Password = password
            };
            return await userService.LoginAsync(u);
        }
    }
}