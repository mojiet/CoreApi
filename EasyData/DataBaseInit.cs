﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EasyEntity.Sys;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using EasyCommon.Extentions;
namespace EasyData
{
    public class DataBaseInit
    {
       
        private static readonly DateTime Now = new DateTime(2018, 6, 7, 0, 0, 0);

        public async Task SeedAsync(CoreDbContext context, IServiceProvider service)
        {
            await InitDB(service);
        }
        /// <summary>
        /// 获取菜单的基础按钮
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        static List<Menu> GetMenuPermision(int parentId, string controllerName)
        {
            return new List<Menu>
            {
                new Menu
                {
                    ParentId = parentId,
                    Name = "添加",
                    Path = "",
                    MenuType = 3,
                    CreateDate = Now,
                    CreateBy = 1,
                    PermisionCode = $"{controllerName}-Add",
                    IsSystemData = true,
                    Order = 1
                },
                new Menu
                {
                    ParentId = parentId,
                    Name = "修改",
                    Path = "",
                    MenuType = 3,
                    CreateDate = Now,
                    CreateBy = 1,
                    IsSystemData = true,
                    PermisionCode = $"{controllerName}-Edit",
                    Order = 2
                },
                new Menu
                {
                    ParentId = parentId,
                    Name = "删除",
                    Path = "",
                    MenuType = 3,
                    CreateDate = Now,
                    CreateBy = 1,
                    IsSystemData = true,
                    PermisionCode = $"{controllerName}-Delete",
                    Order = 3
                }
            };
        }

        public static async Task InitDB(IServiceProvider service)
        {
            using (var serviceScope = service.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<CoreDbContext>();

                if (context.Database != null /*&& context.Database.EnsureCreated()*/)
                {
                    #region 初始化用户

                    var admin = new User
                    {
                        LoginName = "admin",
                        RealName = "超级管理员",
                        Password = "123456".ToMD5(),
                        Email = "JSys@JSys.com",
                        Status = 2,
                        IsSystemData = true,
                        CreateDate = Now
                    };
                    var guest = new User
                    {
                        LoginName = "guest",
                        RealName = "游客",
                        Password = "123456".ToMD5(),
                        Email = "113456@JSys.com",
                        Status = 2,
                        CreateDate = Now,
                        IsSystemData = true
                    };
                    //用户
                    var user = new List<User>
                    {
                        admin,
                        guest
                    };

                    #endregion

                    #region 初始化菜单

                    var system = new Menu
                    {
                        ParentId = 0,
                        Name = "系统设置",
                        Path = "",
                        MenuType = 1,
                        IsSystemData = true,
                        CreateDate = Now,
                        CreateBy = 1,
                        Order = 1
                    };
                    var menuMgr = new Menu
                    {
                        ParentId = 1,
                        Name = "菜单管理",
                        Path = "menu/list",
                        MenuType = 2,
                        CreateDate = Now,
                        CreateBy = 1,
                        IsSystemData = true,
                        Component = "sys/menu-manage/menuManage",
                        Order = 2
                    };//2
                    var roleMgr = new Menu
                    {
                        ParentId = 1,
                        Name = "角色管理",
                        Path = "role/list",
                        MenuType = 2,
                        CreateDate = Now,
                        CreateBy = 1,
                        IsSystemData = true,
                        Component = "sys/role-manage/roleManage",
                        Order = 3
                    };//3
                    var userMgr = new Menu
                    {
                        ParentId = 1,
                        Name = "用户管理",
                        Path = "user/list",
                        MenuType = 2,
                        CreateDate = Now,
                        CreateBy = 1,
                        IsSystemData = true,
                        Component = "sys/user-manage/userManage",
                        Order = 4
                    };//4
                    var sysLog = new Menu()
                    {
                        ParentId = 1,
                        Name = "系统日志",
                        Path = "syslog/list",
                        MenuType = 2,
                        CreateDate = Now,
                        CreateBy = 1,
                        IsSystemData = true,
                        Component = "sys/syslog-manage/syslogManage",
                        Order = 5
                    };//5
                    var menus = new List<Menu>
                    {
                        system,
                        menuMgr,
                        roleMgr,
                        userMgr,
                        sysLog
                    };

                    var menuPermision = GetMenuPermision(2, "Menus");//8
                    var userPermision = GetMenuPermision(3, "Users");//11
                    var rolePermision = GetMenuPermision(4, "Roles");//14
                    menus.AddRange(menuPermision);
                    menus.AddRange(userPermision);
                    menus.AddRange(rolePermision);


                    #endregion

                    #region 初始化角色

                    var superAdminRole = new Role { Name = "超级管理员", Description = "超级管理员", IsSystemData = true, CreateBy = 1 };
                    var guestRole = new Role { Name = "guest", Description = "游客", IsSystemData = true, CreateBy = 1 };
                    var roles = new List<Role>
                    {
                        superAdminRole,
                        guestRole
                    };

                    #endregion

                    #region 用户角色关系

                    var userRoles = new List<UserRole>
                    {
                        new UserRole { UserId = 1, RoleId = 1, IsSystemData = true,CreateBy = 1},
                        new UserRole { UserId = 2, RoleId = 2, IsSystemData = true,CreateBy = 1}
                    };

                    #endregion

                    #region 角色菜单权限关系
                    //超级管理员授权/游客授权
                    var roleMenus = new List<RoleMenu>();
                    var len = menus.Count;
                    for (var i = 0; i < len; i++)
                    {
                        roleMenus.Add(new RoleMenu { RoleId = 1, MenuId = i + 1 });
                        //roleMenus.Add(new RoleMenu { RoleId = 2, MenuId = i + 1 });
                    }

                    #endregion

                    if (!context.Users.Any())  context.Users.AddRange(user);
                    if (!context.Menus.Any()) context.Menus.AddRange(menus);
                    if (!context.Roles.Any()) context.Roles.AddRange(roles);
                    if (!context.UserRoles.Any()) context.UserRoles.AddRange(userRoles);
                    if (!context.RoleMenus.Any()) context.RoleMenus.AddRange(roleMenus);

                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
